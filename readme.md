## Filip Laravel

## Ako spravit projekt?

``git clone https://MichalGallovic@bitbucket.org/MichalGallovic/filiplaravel.git nazovPriecinka;`` //naklonuje projekt do priecinka "nazovPriecinka";  
``cd nazovPriecinka;`` //vojdes do daneho priecinka    
``composer install;`` //nainstalujes s kompozerom vsetky potrebne veci co su v "required" v composer.json  

## Laravel prikazy
Vsetky laravel prikazy sa zacinaju slovickom 'artisan'.  
Ak si nebudes pamatat aka je syntax laravel prikazov tak...  
``php artisan;`` //vypises vsetky mozne prikazy laravelu

### Migracie
``php artisan migrate:make nazovTabulky;`` //vytvori migraciu, pre danu tabulku  
``php artisan migrate;`` //migruje danu tabulku do Databazy, nezabudni migraciu predtym naplnit udajmi o tabulke, ktoru chces vytvorit.  
  
Ak chces sa vratit do stavu databaze spred poslednej migracie tak...  
``php artisan migrate:rollback;``

## Kde čo je ?
app/routes.php - registrovanie ciest pre webovu aplikacu  
app/config/database.php - informacie o nastaveni databaze !nezabudni si cez phpMyAdmina vytvorit svoju databazu na kompe a zmenit tento subor na zaklade tej databazy, co si si vytvoril  
app/database/migrations - tebou vytvorene migracie  
app/controllers - controllery  
app/models - objekty, ktore namapujes na tabulky v databaze ! ak sa tabulka vola "games", tak model bude Game  
app/views - tvoje zobrazenia  


## Čo sme robili
### 7.novembra
- routy
- controlleri
- blade template syntax
- jednoduchy formular



