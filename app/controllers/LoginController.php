<?php

class LoginController extends BaseController {


	public function getIndex() {
		$user = Sentry::getUser();

		return View::make('site.index')->with(compact('user'));
	}

	public function getLogout() {
		Sentry::logout();
		return Redirect::to('login');
	}

	public function getLogin() {
		return View::make('site.login');
	}

	public function postLogin() {
		try
		{
		    // Set login credentials
		    $credentials = array(
		        'email'    => Input::get('email'),
		        'password' => Input::get('password')
		    );

		    // Try to authenticate the user
		    $user = Sentry::authenticate($credentials, false);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    echo 'Wrong password, try again.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User was not found.';
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		    echo 'User is not activated.';
		}

		// The following is only required if throttle is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    echo 'User is suspended.';
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    echo 'User is banned.';
		}

		return Redirect::to('/');
	}

	public function getRegistracia() {
		return View::make('site.registracia');
	}

	public function postRegistracia() {
		$email = Input::get('email');
		$password = Input::get('password');

		try
		{
		    // Create the user
		    $user = Sentry::createUser(array(
		        'email'    => $email,
		        'password' => $password,
		    ));

		    // Find the group using the group id
		    $adminGroup = Sentry::findGroupById(2);

		    // Assign the group to the user
		    $user->addGroup($adminGroup);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    echo 'User with this login already exists.';
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    echo 'Group was not found.';
		}
	}
}