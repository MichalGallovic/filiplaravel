<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::group(array('prefix'	=>	'/','before'	=>	'auth'),function(){
	Route::get('/', 'LoginController@getIndex');
});

Route::controller('/','LoginController');

// Route::controller('/','LoginController');

Route::get('blog/{name}',array('uses'	=>	'PrikladController@getPriklad'));
// Route::controller('site','PrikladController');

